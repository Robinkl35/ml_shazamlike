/* Load the HTTP library */
var http = require("http");
/* Create an HTTP server to handle responses */

http.createServer(function(request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write("Hello World");
  response.end();
}).listen(8888);

app.get('/login', function(req, res) {
var scopes = 'user-read-private user-read-email';
var my_client_id = 'b23c62b7a0f84de481e9c5db536eff78';
var redirect_uri = 'http://robin.lpweb-lannion.fr:8888/';

res.redirect('https://accounts.spotify.com/authorize' +
  '?response_type=code' +
  '&client_id=' + my_client_id +
  (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
  '&redirect_uri=' + encodeURIComponent(redirect_uri));
});
