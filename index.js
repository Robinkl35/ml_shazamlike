var express = require('express');
app = express();
port = process.env.PORT || 8080;

var routes = require('./routes.js');
routes(app);

app.listen(port)