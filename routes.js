'use strict';

module.exports = function (app) {

   var controller = require("./test");

   app.route('/test')
      .get(function (req, res) {res.end('It werkz !')});

   app.route('/api/recon')
      .get(controller.search);
}